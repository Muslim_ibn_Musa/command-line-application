package services;

import models.Child;

public interface TreeOutputService {
    /**
     * Prints all directions and files if searchInput is empty.
     * And prints directions and files by searchInput if it is not empty.
     *
     * @param child       every child of the tree structure which name we print.
     * @param searchInput value can be empty.
     * @param isRegex     boolean which helps understand is the searchInput regex or just string.
     */
    void findFilesAndDirsPaths(Child child, String searchInput, boolean isRegex);
}
