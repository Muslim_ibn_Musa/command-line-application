package services.impl;

import models.Child;
import services.TreeOutputService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TreeOutputServiceImpl implements TreeOutputService {

    @Override
    public void findFilesAndDirsPaths(Child child, String input, boolean isRegex) {
        if (input.isEmpty() || (child.getName().contains(input) && !isRegex)) {
            System.out.println(child.getName());
        } else if (isRegex) {
            Pattern pattern = Pattern.compile(input);
            Matcher matcher = pattern.matcher(child.getName());

            if (matcher.find()) {
                System.out.println(child.getName());
            }
        }

        for (int i = 0; i < child.getChildren().size(); i++) {
            if (!child.getChildren().isEmpty()) {
                findFilesAndDirsPaths(child.getChildren().get(i), input, isRegex);
            }
        }
    }

}
