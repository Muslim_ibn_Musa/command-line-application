package services.impl;

import models.Child;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import services.FileParserService;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XmlFileParserServiceImpl implements FileParserService {

    @Override
    public Child parseFile(String xmlFilePath) throws FileNotFoundException {
        Child tree = new Child();

        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            Document doc = dbf.newDocumentBuilder().parse(new File(xmlFilePath));
            NodeList node = doc.getElementsByTagName("node").item(0).getChildNodes();

            tree = parse(node, "");
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException();
        } catch (ParserConfigurationException | IOException | SAXException ex) {
            ex.printStackTrace();
        }

        return tree;
    }

    /**
     * Recursively parsing the xml file.
     *
     * @param childNodes next child elements of the children element.
     * @param parentName direction/file path of the parent.
     * @return Child class with tree structure.
     */
    private static Child parse(NodeList childNodes, String parentName) {
        Child parent = new Child();
        List<Child> children = new ArrayList<>();

        for (int i = 0; i < childNodes.getLength(); i++) {

            if (childNodes.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }

            switch (childNodes.item(i).getNodeName()) {
                case "name": {
                    parentName = concatNames(parentName, childNodes.item(i).getTextContent());
                    parent.setName(parentName);
                    break;
                }
                case "children": {
                    NodeList parentChildNodes = childNodes.item(i).getChildNodes();
                    for (int j = 0; j < parentChildNodes.getLength(); j++) {
                        if (parentChildNodes.item(j).getNodeType() != Node.ELEMENT_NODE) {
                            continue;
                        }

                        Element element = (Element) parentChildNodes.item(j);
                        boolean isFile = Boolean.parseBoolean(element.getAttribute("is-file"));
                        String name = concatNames(parentName, element.getElementsByTagName("name").item(0).getTextContent());

                        if (isFile) {
                            children.add(new Child(name, isFile));
                        } else {
                            children.add(parse(element.getChildNodes(), parentName));
                        }
                    }
                    break;
                }
            }
        }

        parent.setChildren(children);
        return parent;
    }

    /**
     * Concatenates parentName and childName.
     *
     * @param parentName direction/file path of the parent Child element.
     * @param childName  direction/file path of the current Child element.
     * @return concatenated names.
     */
    private static String concatNames(String parentName, String childName) {
        return parentName.isEmpty() || parentName.endsWith("/")
                ? parentName + childName
                : parentName + "/" + childName;
    }
}
