package services;

import models.Child;

import java.io.FileNotFoundException;

public interface FileParserService {
    /**
     * Implementation of this method parse files as xml etc.
     *
     * @param arg   is an args[1] value
     * @return the Child tree.
     */
    Child parseFile(String arg) throws FileNotFoundException;
}
