package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Child {
    private String name;
    private boolean isFile;
    private List<Child> children = new ArrayList<>();

    public Child(String name, boolean isFile) {
        this.name = name;
        this.isFile = isFile;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public void setChildren(Child children) {
        this.children.add(children);
    }
}
