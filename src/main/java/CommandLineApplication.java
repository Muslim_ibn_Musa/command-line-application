import exceptions.IncorrectInputException;
import models.Child;
import services.FileParserService;
import services.TreeOutputService;
import services.impl.TreeOutputServiceImpl;
import services.impl.XmlFileParserServiceImpl;
import utils.Parameters;

import java.io.FileNotFoundException;

public class CommandLineApplication {

    private static final FileParserService commandLineService = new XmlFileParserServiceImpl();
    private static final TreeOutputService treeOutputService = new TreeOutputServiceImpl();

    public static void main(String[] args) throws FileNotFoundException, IncorrectInputException {
        validateArguments(args);

        Child parent = commandLineService.parseFile(args[1]);

        if (args.length == 4) {
            if (args[2].equals(Parameters.SEARCH_PARAM.value)) {
                treeOutputService.findFilesAndDirsPaths(parent, args[3], false);
            } else if (args[2].equals(Parameters.SEARCH_REGEX_PARAM.value)) {
                treeOutputService.findFilesAndDirsPaths(parent, args[3], true);
            }
        } else {
            treeOutputService.findFilesAndDirsPaths(parent, "", false);
        }
    }

    /**
     * Validating arguments.
     *
     * @param args array received from outside.
     */
    private static void validateArguments(String[] args) throws IncorrectInputException {
        if (args.length >= 2) {
            String param = args[0];
            String arg = args[1];

            if (!param.equals(Parameters.FILE_PARAM.value) || param.isEmpty() || arg.isEmpty())
                throw new IncorrectInputException("Incorrect input. Example: -f <path to xml file>");

            if (args.length == 4) {
                param = args[2];
                arg = args[3];

                if (param.isEmpty() || arg.isEmpty()) {
                    throw new IncorrectInputException("Incorrect input. Example: -f <path to xml file> -s <search input>");
                } else if (!param.equals(Parameters.SEARCH_PARAM.value) && !param.equals(Parameters.SEARCH_REGEX_PARAM.value)) {
                    throw new IncorrectInputException("Incorrect input. Example: -f <path to xml file> -s <search input>");
                }

                if (arg.contains("*") && !param.equals(Parameters.SEARCH_REGEX_PARAM.value)) {
                    args[3] = arg.replace("*", "");
                }
            }
        }
    }

}
