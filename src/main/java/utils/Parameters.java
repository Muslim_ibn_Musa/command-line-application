package utils;

public enum Parameters {
    FILE_PARAM("-f"),
    SEARCH_PARAM("-s"),
    SEARCH_REGEX_PARAM("-S");

    public final String value;

    Parameters(String value) {
        this.value = value;
    }
}
