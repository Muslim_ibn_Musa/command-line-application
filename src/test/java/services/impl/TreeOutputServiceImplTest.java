package services.impl;

import models.Child;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = TreeOutputServiceImpl.class)
class TreeOutputServiceImplTest {

    private static Child testChildTree = new Child("/koren", false);

    @Spy
    @InjectMocks
    TreeOutputServiceImpl treeOutputService;

    @BeforeAll
    static void init() {
        List<Child> children = new ArrayList<>();
        children.add(new Child("/koren/file-776194140.xml", true));
        children.add(new Child("/koren/file-776194141.xml", true));
        children.add(new Child("/koren/file-776194142.xml", true));
        testChildTree.setChildren(children);
        children.add(new Child("/koren/dir-880176375", false));

        List<Child> children1 = new ArrayList<>();
        children1.add(new Child("/koren/dir-880176375/file-1073842118.java", true));
        children1.add(new Child("/koren/dir-880176375/file-1073842117.java", true));
        children1.add(new Child("/koren/dir-880176375/dir-2145307015", false));
        testChildTree.getChildren().get(3).setChildren(children1);

        List<Child> children2 = new ArrayList<>();
        children2.add(new Child("/koren/dir-880176375/dir-2145307015/file-1498940214.xhtml", true));
        children2.add(new Child("/koren/dir-880176375/dir-2145307015/file-1293807724.java", true));
        children2.add(new Child("/koren/dir-880176375/dir-2145307015/dir-1588791577", false));
        testChildTree.getChildren().get(3).getChildren().get(2).setChildren(children2);


        List<Child> children3 = new ArrayList<>();
        children3.add(new Child("/koren/dir-880176375/dir-2145307015/dir-1588791577/file-1272239243.txt", true));
        children3.add(new Child("/koren/dir-880176375/dir-2145307015/dir-1588791577/file-1861587310.txt", true));
        children3.add(new Child("/koren/dir-880176375/dir-2145307015/dir-1588791577/file-780987816.java", true));
        children3.add(new Child("/koren/dir-880176375/dir-2145307015/dir-1588791577/file-50202655.xml", true));
        children3.add(new Child("/koren/dir-880176375/dir-2145307015/dir-1588791577/file-1498565329.txt", true));
        children3.add(new Child("/koren/dir-880176375/dir-2145307015/dir-1588791577/file-1071300276.xhtml", true));
        testChildTree.getChildren().get(3).getChildren().get(2).getChildren().get(2).setChildren(children3);
    }

    @Test
    void successfully_find_all_files_and_dirs_test() {
        treeOutputService.findFilesAndDirsPaths(testChildTree, "", false);
        verify(treeOutputService).findFilesAndDirsPaths(testChildTree, "", false);
    }

    @Test
    void successfully_find_certain_files_test() {
        treeOutputService.findFilesAndDirsPaths(testChildTree, "*.java", false);
        verify(treeOutputService).findFilesAndDirsPaths(testChildTree, "*.java", false);
    }

    @Test
    void successfully_find_certain_dirs_test() {
        treeOutputService.findFilesAndDirsPaths(testChildTree, "158", false);
        verify(treeOutputService).findFilesAndDirsPaths(testChildTree, "158", false);
    }

    @Test
    void successfully_find_certain_files_and_dirs_with_regex_test() {
        treeOutputService.findFilesAndDirsPaths(testChildTree, "(\\w+)-?(\\d+)\\/?(\\w+)-?(\\d+)(.java)", true);
        verify(treeOutputService).findFilesAndDirsPaths(testChildTree, "(\\w+)-?(\\d+)\\/?(\\w+)-?(\\d+)(.java)", true);
    }

    @Test
    void fail_test_throws_null_pointer_exception() {
        assertThrows(NullPointerException.class, () -> treeOutputService.findFilesAndDirsPaths(null, "", false));
        verify(treeOutputService).findFilesAndDirsPaths(null, "", false);
    }

}