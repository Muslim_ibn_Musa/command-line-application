import exceptions.IncorrectInputException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes = CommandLineApplication.class)
class CommandLineApplicationTest {
    private final String FILE_PATH = "src/main/resources/test.xml";

    @Test
    void main_test_successfully_run_without_search_input() throws FileNotFoundException, IncorrectInputException {
        CommandLineApplication.main(
                new String[]{"-f", FILE_PATH});
    }

    @Test
    void main_test_successfully_run_with_searching_certain_file() throws FileNotFoundException, IncorrectInputException {
        CommandLineApplication.main(
                new String[]{"-f", FILE_PATH,
                        "-s", "file-1102113691.java"});
    }

    @Test
    void main_test_successfully_run_with_searching_all_files_of_java_format() throws FileNotFoundException, IncorrectInputException {
        CommandLineApplication.main(
                new String[]{"-f", FILE_PATH,
                        "-s", "*.java"});
    }

    @Test
    void main_test_successfully_run_with_regex_searching() throws FileNotFoundException, IncorrectInputException {
        CommandLineApplication.main(
                new String[]{"-f", FILE_PATH,
                        "-S", "(\\w+)-?(\\d+)\\/?(\\w+)-?(\\d+)(.java)"});
    }

    @Test
    void fail_main_test_file_not_found_exception() {
        assertThrows(FileNotFoundException.class, () -> {
                CommandLineApplication.main(
                        new String[]{"-f", "test/"+FILE_PATH,
                                "-S", "(\\w+)-?(\\d+)\\/?(\\w+)-?(\\d+)(.java)"});
        });
    }

    @Test
    void fail_main_test_incorrect_filepath_param() {
        assertThrows(IncorrectInputException.class, () -> {
                CommandLineApplication.main(
                        new String[]{"-F", "test/"+FILE_PATH,
                                "-s", "*.java"});
        });
    }

    @Test
    void fail_main_test_incorrect_search_param() {
        assertThrows(IncorrectInputException.class, () -> {
                CommandLineApplication.main(
                        new String[]{"-f", "test/"+FILE_PATH,
                                "-c", "*.java"});
        });
    }

}