# command-line-application

Java Developer test assignment
Introduction
You are provided with xml file containing a representation of files hierarchy.

Example:

![img.png](images/img.png)

One node may be either file or directory, which is defined by attribute _is-file_

What to do?

Implement command-line application which will output all full paths for the given input.

$ java -jar assignment.jar -f <xml_file> -s <search_input>

where <xml_file> is a path to the provided xml file; 
<search_input> is search string to filter paths with.

### **Examples**

No search input:

$ java -jar assignment.jar -f test-files.xml

![img_1.png](images/img_1.png)

Exact search input:

$ java -jar assignment.jar -f test-files.xml -s file-1498940214.xhtml

![img_3.png](images/img_3.png)

Simple search input:

$ java -jar assignment.jar -f test-files.xml -s ‘*.java’

![img_5.png](images/img_5.png)

Extended search input:

$ java -jar assignment.jar -f test-files.xml -S ‘.*?[a-z]{4}-\\d+\.[a-z]+’

![img_4.png](images/img_4.png)

## **Compilation and test environment**

Prepare Maven project. It is allowed to use any libraries you might find useful; Java SE 8+ or
Kotlin.

Build should run without problems with Maven 3.5.0.